package oop;

public class PublicTransport  {
    private int averageSpeed;
    private int price;
    private int comfortable;

    public PublicTransport(int averageSpeed, int price, int comfortable) {
        this.averageSpeed=averageSpeed;
        this.price=price;
        this.comfortable=comfortable;
    }
    public int AverageQuality(){
        return ((this.averageSpeed + this.price + this.comfortable) / 3);
    }
    public String priceCounter(){
        if (this.price > 20){
            return "luxury";
        }
        return "cheap";
    }


}
