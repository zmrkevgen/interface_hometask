package oop;

public abstract class DriverWarning {
    public abstract  void stationName();
    public abstract void busStationName();
    public abstract void taxiDriverMessage();
}
