package oop;

public class Bus extends PublicTransport implements DisplayPrice{

    public String place;

    public Bus(int averageSpeed, int price, int comfortable, String place  ) {
        super(averageSpeed, price, comfortable);
        this.place=place;
    }

    @Override
    public String priceCounter() {
        return super.priceCounter();
    }

    public String getPlace(){

        return place;
    }
    public void setPlace(String place){
       this.place=place;
    }


    @Override
    public void message() {
        System.out.println("good luck");
    }
}
