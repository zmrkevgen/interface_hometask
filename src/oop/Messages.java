package oop;

public class Messages extends DriverWarning {
    @Override
    public void stationName() {
        System.out.println("Stantsia Puskins'ka");
    }

    @Override
    public void busStationName() {
        System.out.println("Ploshcha Svobody");
    }

    @Override
    public void taxiDriverMessage() {
        System.out.println("Driving is over");
    }
}
